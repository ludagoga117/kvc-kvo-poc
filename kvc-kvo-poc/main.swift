//
//  main.swift
//  kvc-kvo-poc
//
//  Created by Luis David Goyes Garces on 11/11/19.
//  Copyright © 2019 Luis David Goyes Garces. All rights reserved.
//

import Foundation

final class Car: NSObject {
    @objc var model: String!
}

final class Person: NSObject {
    @objc var name: String!
    @objc var age: Int = 0
    // What happens when you use Int!?
    @objc var car: Car = Car()
}

var david = Person()

david.setValue("David", forKey: "name")
print(david.value(forKey: "name") as! String)

david.setValue("25", forKey: "age")
print(david.value(forKey: "age") as! Int)

david.setValue("Kicks", forKeyPath: "car.model")
print(david.value(forKeyPath: "car.model") as! String)

// What happens when you use an incorrect key?

// KVO
// KVO classes must be KVC complient and must be able to send notifications

//class DynamicPerson: NSObject {
//    @objc dynamic var name: String!
//}
//
//final class Observer: NSObject {
//
//    dynamic var person: DynamicPerson!
//
//
//    func subscribe(person: DynamicPerson) {
//        self.person = person
//        self.person.addObserver(self, forKeyPath: "name", options: [.new, .old], context: nil)
//    }
//
//    override class func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
//
//        if keyPath == "name" {
//            DispatchQueue.main.async {
//                print("La persona cambio de nombre de \(change![.newKey] as! String) a \(change![.oldKey] as! String)")
//            }
//        }
//    }
//
//    deinit {
//        person.removeObserver(self, forKeyPath: "name")
//        person = nil
//    }
//}
//
//var dynamicDavid = DynamicPerson()
//dynamicDavid.name = "David"
//
//dynamic var observer = Observer()
//observer.subscribe(person: dynamicDavid)
//
//dynamicDavid.name = "Luis"
//
//sleep(10)

